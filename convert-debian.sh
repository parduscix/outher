#!/bin/bash
set -e

## Please remove plymouth before process

if [[ $UID -ne 0 ]] ; then
    echo "You must be root!"
    exit 1
fi

missing=()
for cmd in make wget tar ar ; do
    if ! which $cmd &>/dev/null ; then
        missing+=($cmd)
    fi
done
if [[ "${missing[@]}" != "" ]] ; then
    echo "Missing: ${missing[@]}" >/dev/stderr
    exit 1
fi

cd /tmp
# First step: install static busybox
pkg=$(wget https://deb.debian.org/debian/pool/main/b/busybox/ -O -  | grep "amd64" | \
       sed "s/.*href=\"//g;s/\".*//g" | grep "busybox-static" | sort | tail -n 1)
wget "https://deb.debian.org/debian/pool/main/b/busybox/$pkg" -O /tmp/busybox.deb
ar x /tmp/busybox.deb
tar -xf /tmp/data.tar.xz
cp /tmp/bin/busybox /busybox
rm -rf /tmp/*

# Step 2: install debootstrap
if ! which debootstrap ; then
    wget https://salsa.debian.org/installer-team/debootstrap/-/archive/master/debootstrap-master.zip
    unzip debootstrap-master.zip
    make install -C debootstrap-master
fi

# Step 3 install base system
if [[ ! -d /rootfs ]] ; then
    debootstrap --no-merged-usr --no-check-gpg testing /rootfs
    chroot /rootfs apt-get install busybox-static -yq
fi

# Step 4 backup bootloader and kernel
if [[ -d /save ]] ; then
    rm -rf /save
fi
mkdir -p /save
cp -prf /boot /save/
cp -prfv /usr/lib/grub /save/grub-lib
cp -prfv /usr/share/grub /save/grub-share
USERNAME=$(grep 1000 /etc/passwd | cut -f 1 -d ":")
echo "$USERNAME" > /save/user
grep -e "^$USERNAME:" /etc/shadow >> /save/user
cp -pf /etc/fstab /save/fstab

# Step 5 install desktop environment (gnome)
chroot /rootfs apt install xinit xserver-xorg -yq
chroot /rootfs apt install gnome-core -yq

mkdir -p /old
for dir in bin usr etc lib lib32 libx32 lib64 opt srv sbin var ; do
    rm -f /$dir 2>/dev/null || true
    [[ -d /$dir ]] && /busybox mv /$dir /old/
    [[ -d /rootfs/$dir ]] && /busybox mv /rootfs/$dir /
done
# restore files
cp -prf /save/fstab /etc/fstab
cp -prf /save/grub-lib /usr/lib/grub
cp -prf /save/grub-share /usr/share/grub
cp -prf /save/boot /boot
useradd $USERNAME
cat /save/user | tail -n 1 >> /etc/shadow
